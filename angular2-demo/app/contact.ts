
import { Injectable } from 'angular2/core';

@Injectable()
export class Contact {
  constructor(
    public firstname: string,
    public lastname: string,
    public country: string,
    public phone: number
  ) {  }
}
package dto;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity
public class Movie {

	@GraphId
	private Long id;

	private String tagline;

	private String title;

	private int released;

	@Relationship(type = "ACTED_IN", direction = Relationship.INCOMING)
	private List<ActedIn> roles = new ArrayList<>();

	// @Relationship(type = "DIRECTED")
	// private Person director;

	public Movie() {
	}

	public Movie(String title, int released) {
		this.title = title;
		this.released = released;
	}

	public Long getId() {
		return id;
	}

	public String getTagline() {
		return tagline;
	}

	public String getTitle() {
		return title;
	}

	public int getReleased() {
		return released;
	}

	public List<ActedIn> getRoles() {
		return roles;
	}
}

package dto;

import java.util.ArrayList;
import java.util.Collection;

import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = "ACTED_IN")
public class ActedIn {

	@GraphId
	private Long id;
	
	@StartNode
	private Person person;

	@EndNode
	private Movie movie;
	
	//
	private Collection<String> roles = new ArrayList<>();

	public ActedIn() {
	}

	public ActedIn(Movie movie, Person actor) {
		this.movie = movie;
		this.person = actor;
	}

	public Long getId() {
		return id;
	}

	public Person getPerson() {
		return person;
	}

	public Movie getMovie() {
		return movie;
	}

	public Collection<String> getRoles() {
		return roles;
	}
	
	public void addActedInName(String name){
		this.roles.add(name);
	}
	
}

package repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import dto.Person;

@Repository
public interface PersonRepository extends PagingAndSortingRepository<Person, Long> {

}

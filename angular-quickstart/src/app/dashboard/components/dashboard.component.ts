
import { Component, OnInit } from '@angular/core';

// Hero imports
import { Hero }        from '../../hero/DTO/hero';
import { HeroService } from '../../hero/services/hero.service';

@Component({
  moduleId: module.id,
  selector: 'my-dashboard',
  templateUrl: '../templates/dashboard.html',
  styleUrls: ['../static/dashboard.css'],
})

export class DashboardComponent implements OnInit { 

  heroes : Hero[] = [];

  constructor(private heroService: HeroService) { }

  ngOnInit(): void {
    this.heroService.getHeroes()
      .then(heroes => this.heroes = heroes.slice(1,5));
  }
}

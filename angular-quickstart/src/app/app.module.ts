
//System imports
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';

//App imports
import { AppComponent }        from './app.component';
import { AppRoutingModule }    from './app-routing.module';

//Hero imports
import { HeroDetailComponent } from './hero/components/hero-detail.component';
import { HeroesComponent }     from './hero/components/heroes.component';
import { HeroSearchComponent } from './hero/components/hero-search.component'
import { HeroService }         from './hero/services/hero.service';

//Dashboard impots
import { DashboardComponent } from './dashboard/components/dashboard.component';

// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

//primeNG imports
import { ButtonModule }    from '../../node_modules/primeng/primeng';
import { PanelMenuModule } from '../../node_modules/primeng/primeng';


@NgModule({
  imports:  [ 
    BrowserModule, 
    FormsModule, 
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService),
    AppRoutingModule,
    ButtonModule,
    PanelMenuModule,
    ],
  declarations: [ 
    AppComponent, 
    DashboardComponent,
    HeroDetailComponent, 
    HeroesComponent,
    HeroSearchComponent,
  ],
  providers:    [ HeroService ],
  bootstrap:    [ AppComponent ],
})

export class AppModule { }


//System imports
import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';

//Hero imports
import { HeroService } from '../services/hero.service';
import { Hero }        from '../DTO/hero';

import {ButtonModule} from 'primeng/primeng';

@Component({
  moduleId: module.id,
  selector: 'my-heroes',
  templateUrl: '../templates/hero-component.html',
  styleUrls: ['../static/hero-component.ccs'],
})

//
export class HeroesComponent implements OnInit { 

  heroes: Hero[] = [];
  selectedHero: Hero;

  //Creates services
  constructor(
    private heroService: HeroService,
    private router: Router) {}

  //Init services
  ngOnInit(): void {
    this.getHeroes();
  }
 
   //Get all heroes
  getHeroes(): void {
      this.heroService.getHeroes().
        then(heroes => this.heroes = heroes)
  }

  //Selected Heroe
	onSelect(hero: Hero): void {
		this.selectedHero = hero;
	}

  //Navigate to the details of the hero
  gotoDetail(): void{
    this.router.navigate(['/detail', this.selectedHero.id]);
  }

  //Add a hero 
  add(name: String): void{
    name = name.trim();
    if (!name){
      return;
    }

    this.heroService.create(name).
      then(hero => {
        this.heroes.push(hero);
        this.selectedHero = null;
      });
  }

  //Delete a Hero
  delete(hero: Hero): void{
    this.heroService.delete(hero.id)
      .then(() => {
        this.heroes = this.heroes.filter(h => h !== hero);
        if (this.selectedHero == hero ){
          this.selectedHero = null;
        }
      })
  }
}

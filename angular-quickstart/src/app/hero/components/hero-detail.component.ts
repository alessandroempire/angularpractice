
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import 'rxjs/add/operator/switchMap';

//Hero imports
import { HeroService } from '../services/hero.service';
import { Hero } 	     from '../DTO/hero';

@Component({
  moduleId: module.id,
  selector: 'my-hero-detail',
  templateUrl: '../templates/hero-detail.html',
  styleUrls: ['../static/hero-detail.css'],
})

export class HeroDetailComponent implements OnInit{

	hero: Hero; 

  constructor(
    private heroService: HeroService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  //Init
  ngOnInit(): void {
    this.route.params
      .switchMap((params: Params) => this.heroService.getHero(+params['id']))
      .subscribe(hero => this.hero = hero);
  }

  //Navigates backward one step in the browsers history stack
  goBack(): void {
    this.location.back();
  }

  //Save hero detail component
  save(): void{
    this.heroService.update(this.hero)
      .then(() => this.goBack());
  }
}

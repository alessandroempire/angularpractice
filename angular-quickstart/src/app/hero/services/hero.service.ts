
//System imports
import { Injectable }          from '@angular/core';
import { Headers, Http }       from '@angular/http';
import 'rxjs/add/operator/toPromise';

//Hero imports
import { Hero }   from '../DTO/hero';
// import { HEROES } from '../mocks/mock-heroes';

@Injectable()
export class HeroService {

  private heroesUrl = 'api/heroes';  // URL to web api
  private headers   = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) { }

  //Get array of ALL heroes
  getHeroes(): Promise<Hero[]> {
    return this.http.get(this.heroesUrl)
     .toPromise()
       .then(response => response.json().data as Hero[])
         .catch(this.handleError);
  } 

  //Get heroe with identifier id
  getHero(id: number): Promise<Hero> {
      const url = `${this.heroesUrl}/${id}`;
      return this.http.get( url )
       .toPromise()
         .then(response => response.json().data as Hero)
           .catch(this.handleError);
  }

  //Update Hero 
  update(hero: Hero): Promise<Hero> {
    const url = `${this.heroesUrl}/${hero.id}`;
    return this.http
      .put(url, JSON.stringify(hero), {headers: this.headers})
        .toPromise()
          .then(() => hero)
            .catch(this.handleError);
  }

  //Create a Hero
  create(name: String): Promise<Hero> {
    return this.http
      .post(this.heroesUrl, JSON.stringify({name: name}), {headers: this.headers})
        .toPromise()
          .then(res => res.json().data)
            .catch(this.handleError);
  }

  //Delete a hero
  delete(id: number): Promise<void> {
    const url = `${this.heroesUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
        .then(() => null)
          .catch(this.handleError);
  }

  //Error handling
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
